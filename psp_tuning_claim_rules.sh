#!/bin/sh
#Clear specific LUN config created by prnxd to allow proper claiming on reboot.
#Author: Andy Daniel
#Created: 10/13/2014
#Last Updated: 12/06/2016 - Modified for >=3.1.0.6 environments where prnxd always flips PSP on init.
#Notes: This script will clear PRNX_PSP config created by prnxd to facilitate standard
#		claim rule config and set claim rules for several AFA vendors.
#Usage: chmod 744 .sh & ./psp_tuning_claim_rules.sh
#       The output is saved in the current working directory as psp_tuning_claim_rules<hostname>_<timestamp>.log 
read -p "This script will stop prnxd and immediately reboot this host. Press y to continue or any other key to exit: " -n1 proceed
if [ "$proceed" != "y" ]; then
echo ""
echo "Script terminated by keystroke."
exit;
fi
echo ""
maintmode=`esxcli system maintenanceMode get`
if [ "$maintmode" != "Enabled" ]; then
echo "Host must be in Maintenance Mode!";
exit;
fi
host=`hostname`
timestamp=`date +"%Y-%m-%dT%H:%M"`
logname=psp_tuning_claim_rules_${host}_${timestamp}.log
echo "Stopping prnxd..."
echo "Stopping prnxd..." > ${logname}
/etc/init.d/prnxd stop
echo "Changing default PSP to VMW_PSP_RR for SATPs..."
echo "Changing default PSP to VMW_PSP_RR for SATPs..." >> ${logname}
esxcli storage nmp satp set -s "VMW_SATP_EQL" -P "VMW_PSP_RR"
esxcli storage nmp satp set -s "VMW_SATP_DEFAULT_AA" -P "VMW_PSP_RR"
esxcli storage nmp satp set -s "VMW_SATP_ALUA" -P "VMW_PSP_RR"
esxcli storage nmp satp set -s "VMW_SATP_SYMM" -P "VMW_PSP_RR"
esxcli storage nmp satp set -s "VMW_SATP_EVA" -P "VMW_PSP_RR"
echo "Adding claim rules..."
echo "Adding claim rules..." >> ${logname}
esxcli storage nmp satp rule add -s "VMW_SATP_EQL" -V "EQLOGIC" -M "100E-00" -P "VMW_PSP_RR" -O "iops=3" -e "FVP EQL Optimized; EQL TR1091" --force
esxcli storage nmp satp rule add -s "VMW_SATP_DEFAULT_AA" -V "3PARdata" -M "VV" -P "VMW_PSP_RR" -O "iops=1" -c tpgs_off -e "FVP 3Par A/A Optimized; HP QL226-97872" --force
esxcli storage nmp satp rule add -s "VMW_SATP_ALUA"  -V "3PARdata" -M "VV" -P "VMW_PSP_RR" -O "iops=1" -c tpgs_on -e "FVP 3Par ALUA Optimized; HP QL226-97872" --force
esxcli storage nmp satp rule add -s "VMW_SATP_ALUA" -V "Nimble" -M "Server" -P "VMW_PSP_RR" -O "iops=1" -e "FVP Nimble ALUA Optimized; Nimble KB-000059" --force
esxcli storage nmp satp rule add -s "VMW_SATP_SYMM" -V "EMC" -M "SYMMETRIX" -P "VMW_PSP_RR" -O "iops=1" -e "FVP VMAX ALUA Optimized; EMC H2529.13" --force
esxcli storage nmp satp rule add -s "VMW_SATP_EVA" -V "HP" -M "HSV*" -P "VMW_PSP_RR" -O "iops=1" -c tpgs_off -e "FVP EVA A/A Optimized; HP 4AA1-2185ENW" --force
esxcli storage nmp satp rule add -s "VMW_SATP_ALUA" -V "HP" -M "HSV*" -P "VMW_PSP_RR" -O "iops=1" -c tpgs_on -e "FVP EVA ALUA Optimized; HP 4AA1-2185ENW" --force
esxcli storage nmp satp rule add -s "VMW_SATP_DEFAULT_AA" -V "XtremIO" -M "XtremApp" -P "VMW_PSP_RR" -O "iops=1" -c tpgs_off -e "FVP XtremIO A/A RR Optimized; EMC 302-001-288" --force
esxcli storage nmp satp rule add -s "VMW_SATP_ALUA" -V "PURE" -M "FlashArray" -P "VMW_PSP_RR" -O "iops=1" -e "FVP Pure ALUA Optimized; Pure vSphere BPG" --force
esxcli storage nmp satp rule add -s "VMW_SATP_ALUA" -V "Skyera" -M "Skyhawk" -P "VMW_PSP_RR" -O "iops=1" -e "FVP Skyera Skyhawk ALUA RR Optimized" --force
esxcli storage nmp satp rule add -s "VMW_SATP_ALUA" -V "NETAPP" -P "VMW_PSP_RR" -c tpgs_on -e "FVP NetApp ALUA" --force
esxcli storage nmp satp rule add -s "VMW_SATP_ALUA" -V "TEGILE" -M "ZEBI-FC" -P "VMW_PSP_RR" -c tpgs_on -e "FVP Tegile FC ALUA; Tegile TCS" --force
esxcli storage nmp satp rule add -s "VMW_SATP_DEFAULT_AA" -V "COMPELNT" -M "Compellent Vol" -P "VMW_PSP_RR" -O "iops=3" -e "FVP Compellent Optimized; Dell 680-041-020" --force
echo "Clearing LUN specific PSP configuration..."
echo "Clearing LUN specific PSP configuration..." >> ${logname}
echo "PRE PSP CONFIG" >> ${logname}
esxcli storage nmp device list >> ${logname}
for i in `esxcli storage nmp device list | grep Display | grep 'naa\|eui' | awk '{print $NF}' | sed 's/(//g'|sed 's/)//g'`; do
esxcli storage nmp psp generic deviceconfig get -d $i >> ${logname};
esxcli storage nmp device set --device $i --default >> ${logname};
esxcli storage nmp psp generic deviceconfig set -d $i -c="" >> ${logname};
done
echo "POST PSP CONFIG" >> ${logname}
esxcli storage nmp device list >> ${logname}
echo "Backing up host config..."
echo "Backing up host config..." >> ${logname}
/sbin/auto-backup.sh >> ${logname}
echo "Rebooting the host..."
echo "Rebooting the host..." >> ${logname}
reboot
